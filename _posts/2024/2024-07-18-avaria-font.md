---
layout: default
title: Avaria font d'alimentació
---
El servidor es troba aturat a conseqüència d'una avaria amb la font d'alimentació.
Esperem poder-ho solucionar dilluns al matí.

**Actualització 2024-07-22:** el problema amb la font d'alimentació va afectar la placa base i hem hagut d'aturar la màquina. Probablement no es podrà restablir el servei fins al setembre.

Us preguem que dispenseu les molèsties que el tall us pugui ocasionar.
